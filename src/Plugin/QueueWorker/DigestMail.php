<?php

namespace Drupal\personal_digest\Plugin\QueueWorker;

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "personal_digest_mail",
 *   title = @Translation("Generate personalised digest mail"),
 *   cron = {"time" = 60}
 * )
 */
class DigestMail extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The Mail Manager service.
   *
   * @var MailManagerInterface
   */
  protected $mailManager;

  /**
   * The user data service.
   *
   * @var UserDataInterface
   */
  protected $userDataStore;

  /**
   * The account switcher service.
   *
   * @var AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param UserDataInterface $user_data
   *   UserData service.
   * @param MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, MailManagerInterface $mail_manager, AccountSwitcherInterface $account_switcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userDataStore = $user_data;
    $this->mailManager = $mail_manager;
    $this->accountSwitcher = $account_switcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.data'),
      $container->get('plugin.manager.mail'),
      $container->get('account_switcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $recipient = User::load($data);
    $settings = $this->userDataStore->get('personal_digest', $data, 'digest');
    $settings['last'] = strtotime('-' . $settings['weeks_interval'] . ' weeks');

    // Check the last sending time again in case something got put in the queue
    // twice.
    if ($settings['last'] < strtotime('today')) {
      $email = $this->buildMail([
        'user' => $recipient,
        'displays'  => $settings['displays'],
        'weeks_interval' => $settings['weeks_interval'],
        'last' => $settings['last'],
      ]);

      if ($email) {
        $this->mailManager
          ->mail('personal_digest',
            'digest',
            $recipient->getEmail(),
            $recipient->getPreferredLangcode(),
            [
              'email' => $email,
              'user' => $recipient,
            ]
          );
      }
      // The mail is sent, so save the last sent time.
      $settings['last'] = \Drupal::service('personal_digest.time')->getRequestTime();
      $this->userDataStore->set('personal_digest', $data, 'digest', $settings);
    }
  }

  /**
   * Build the email digest body.
   *
   * @param array $params
   *   Contain the data to build the email.
   *
   * @return array|false
   *   Return false if the mail is empty.
   */
  private function buildMail($params) {
    $digest_content = [];
    $this->accountSwitcher->switchTo($params['user']);
    foreach ($params['displays'] as $combo => $value) {
      list($view_name, $display_name) = explode(':', $combo);
      $view = Views::getView($view_name);
      if (!$view || !$view->access($display_name)) {
        continue;
      }
      $view->setArguments([date('Y-m-d', $params['last'])]);
      $view->setDisplay($display_name);
      $result = $view->preview();

      if ($view->total_rows == 0) {
        continue;
      }

      $source = \Drupal::service('renderer')->renderPlain($result);
      $digest_content[$combo] = render($source);
    }
    $this->accountSwitcher->switchBack();

    // If the $digest_content is empty, return false to avoid sent empty
    // mails.
    if (empty($digest_content)) {
      return FALSE;
    }

    // Building the email.
    $message['subject'] = t(
      '%site_name digest since @date',
      [
        '%site_name' => \Drupal::config('system.site')->get('name'),
        '@date' => \Drupal::service('date.formatter')->format($params['last'], 'short'),
      ],
      ['langcode' => $params['user']->getPreferredLangcode()]
    );
    $message['body'][] = t(
      'Greetings @name',
      ['@name' => $params['user']->getDisplayName()],
      ['langcode' => $params['user']->getPreferredLangcode()]
    );
    $message['body'] += $digest_content;

    return $message;
  }

}
