<?php

namespace Drupal\personal_digest;

use Drupal\Core\Link;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Drupal\user\Entity\User;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a user password reset form.
 */
class SiteSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'personal_digest_site_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $checkboxes = $options = [];
    foreach (Views::getViewsAsOptions(FALSE, 'enabled') as $key => &$view_display) {
      list($view_id, $display_id) = explode(':', $key);
      $view = View::load($view_id)->getExecutable();
      $view->setDisplay($display_id);
      if ($this->digestible($view)) {
        $options[$key] = $view_display;
      }
    }
    if (\Drupal::moduleHandler()->moduleExists('views_ui')) {
      foreach ($options as $key => $view_display) {
        list($view_id, $display_id) = explode(':', $key);
        $options[$key] = Link::createFromRoute(
          $view_display,
          'entity.view.edit_display_form',
          ['view' => $view_id, 'display_id' => $display_id],
          // Todo open in a new window.
          $options
        )->toString();
      }
    }

    $settings = $this->config('personal_digest.settings');
    $hours = [];
    foreach (range(0, 23) as $number) {
      $hours[$number] = $number . ":00";
    }

    if ($options) {
      $form['views'] = [
        '#title' => $this->t('Available views displays.'),
        '#description' => $this->t("Views displays with a first contextual filter '@arg'.", ['@arg' => 'date_fulldate_since']),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $settings->get('views'),
      ] + $checkboxes;

      $form['include_change_settings_link'] = [
        '#title' => $this->t('Include change settings link in the email.'),
        '#description' => $this->t('Include a link in the mails to let the users changes their mail preferences.'),
        '#type' => 'checkbox',
        '#default_value' => $settings->get('include_change_settings_link')
      ];

      $form['hour'] = [
        '#title' => 'Hour',
        '#description' => 'Select the hour to automatically send the Digest. (The mails will be send in the first cron after this hour)',
        '#type' => 'select',
        '#options' => $hours,
        '#default_value' => $settings->get('hour'),
      ];

      $week_days = [];
      $day_start = date("d", strtotime("next Sunday"));
      for ($x = 0; $x < 7; $x++) {
        $unixtime = mktime(0, 0, 0, date("m"), $day_start + $x, date("y"));
        // Create weekdays array.
        $week_days[date('l', $unixtime)] = date('l', $unixtime);
      }

      $form['defaultdayoftheweek'] = [
        '#title' => 'Default Day of the week',
        '#description' => 'Define the default day when the digest will be send',
        '#type' => 'select',
        '#options' => $week_days,
        '#default_value' => $settings->get('defaultdayoftheweek'),
      ];

      $form['actions'] = [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
        ],
        'test' => [
          '#type' => 'submit',
          '#value' => $this->t('Submit & test'),
          '#submit' => [[$this, 'test']]
        ],
      ];
    }
    else {
      drupal_set_message($this->t("There are no views yet with with a '@name' argument", ['@name' => 'date_fulldate_since']));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('personal_digest.settings')
      ->set('views', array_values(array_filter($values['views'])))
      ->set('include_change_settings_link', $values['include_change_settings_link'])
      ->set('hour', $values['hour'])
      ->set('defaultdayoftheweek', $values['defaultdayoftheweek'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Submit callback
   */
  public function test(array &$form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);
    $recipient = User::load(\Drupal::currentUser()->id());
    \Drupal::service('plugin.manager.mail')
      ->mail('personal_digest',
        'digest',
        $recipient->getEmail(),
        $recipient->getPreferredLangcode(),
        [
          'user' => $recipient,
          'displays' => array_filter($form_state->getValue('views')),
          'weeks_interval' => 4,
          'last' => strtotime('-1 month'),
        ]
      );
    drupal_set_message($this->t('Check your mail'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['personal_digest.settings'];
  }


  /**
   * Check if a views display can be used for digest.
   *
   * A view is suitable if it has the tag 'digest' and an argument based on a time stamp, e.g. created, updated
   *
   * @param string $view
   * @return bool | NULL
   *   TRUE if the display's first arg is date_fulldate_since
   */
  private function digestible($view) {
    if ($args = $view->getHandlers('argument')) {
      $arg = reset($args);
      if ($arg['plugin_id'] == 'date_fulldate_since') {
        return $arg['field'];
      }
    }
  }


}
