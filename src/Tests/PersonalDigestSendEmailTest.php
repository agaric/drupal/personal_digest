<?php

namespace Drupal\personal_digest\Tests;

/**
 * Tests for the email_example module.
 *
 * @group personal_digest
 */
class PersonalDigestSendEmailTest extends PersonalDigestTestBase {

  /**
   * Tests Nodes.
   * @var array
   */
  protected $testsNodes = [];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->testsNodes[] = $this->drupalCreateNode();
    $this->testsNodes[] = $this->drupalCreateNode();
  }

  /**
   * Send a digest.
   */
  public function testSendEmail() {
    // Subscribing the adminUser to get the digest in the next Cron.
    \Drupal::service('user.data')->set(
      'personal_digest',
      $this->adminUser->id(),
      'digest',
      [
        'displays' => ['personal_digest_test:default' => '0'],
        'daysoftheweek' => date('l', strtotime('today')),
        'weeks_interval' => 1,
      ]
    );

    // Correct Day and correct hour, The digest should be send.
    \Drupal::state()->set('personal_digest_time', mktime(9, 0, 0));
    $this->cronRun();
    $mails = \Drupal::state()->get('system.test_mail_collector', []);
    $mail = $mails[0]['body'];

    // Check if the digest contain the created nodes.
    foreach ($this->testsNodes as $node) {
      $this->assertTrue(strstr($mail, $node->getTitle()), 'The ' . $node->getTitle() . ' node was sent');
    }
  }

  /**
   * No mail sent when is not after the configured hour.
   */
  public function testNoHour() {
    // Subscribing the adminUser to get the digest in the next Cron.
    \Drupal::service('user.data')->set(
      'personal_digest',
      $this->adminUser->id(),
      'digest',
      [
        'displays' => ['personal_digest_test:default' => '0'],
        'daysoftheweek' => date('l', strtotime('today')),
        'weeks_interval' => 1,
      ]
    );

    // Correct day, wrong  hour.
    \Drupal::state()->set('personal_digest_time', mktime(7, 0, 0));
    $this->cronRun();

    $mails = \Drupal::state()->get('system.test_mail_collector', []);
    $this->assertTrue(empty($mails), 'No mails were sent because it was\'t the correct hour');
  }

  /**
   * No mail sent when is not the day of the week.
   */
  public function testNoDayOfTheWeek() {
    \Drupal::service('user.data')->set(
      'personal_digest',
      $this->adminUser->id(),
      'digest',
      [
        'displays' => ['personal_digest_test:default' => '0'],
        'daysoftheweek' => date('l', strtotime('tomorrow')),
        'weeks_interval' => 1,
      ]
    );

    // Correct day, wrong day.
    \Drupal::state()->set('personal_digest_time', mktime(9, 0, 0));
    $this->cronRun();

    $mails = \Drupal::state()->get('system.test_mail_collector', []);
    $this->assertTrue(empty($mails), 'No mails were sent because today wasn\'t the correct day to send the digest');
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() {
    parent::tearDown();
    \Drupal::state()->delete('personal_digest_time');
  }

}
