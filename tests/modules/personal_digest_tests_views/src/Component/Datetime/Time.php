<?php

namespace Drupal\personal_digest_tests_views\Component\Datetime;

use Drupal\personal_digest\Component\Datetime\TimeInterface;
use Drupal\personal_digest\Component\Datetime\Time as PersonalDigestTime;

/**
 * Provides a class for obtaining system time.
 */
class Time extends PersonalDigestTime implements TimeInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function getRequestTime() {
    $time = \Drupal::state()->get('personal_digest_time');
    if ($time) {
      return $time;
    }
    return $this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME');
  }

}

