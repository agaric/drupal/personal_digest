<?php

/**
 * @file
 * Contains Drupal\personal_digest_tests_views\PersonalDigestTestsViewsServiceProvider
 */

namespace Drupal\personal_digest_tests_views;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class PersonalDigestTestsViewsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides language_manager class to test domain language negotiation.
    $definition = $container->getDefinition('personal_digest.time');
    $definition->setClass('\Drupal\personal_digest_tests_views\Component\Datetime\Time');
  }
}